import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import '../styles/components/ValidationResultCard.scss';

const NoResult = () => {
  return (<span className="muted">No results</span>);
}

const ResultTable = (result) => {
  if (!result) return null
  const attrRows = Object.keys(result).map((attr) =>
    <tr key={attr}>
      <td className="key">{ attr }</td>
      <td className="value">{ result[attr] }</td>
    </tr>
  );

  return (
    <table className="result table">
      <thead>
        <tr>
          <th>Attribute</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        { attrRows }
      </tbody>
    </table>
  );
}

export const ValidationResultCard = ({ isLoading, result }) => {
  return (
    <div className="validation-result-card card">
      <div className="card-body">
        <h2 className="title">Validation Result</h2>
        { result ? ResultTable(result) : NoResult() }
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    result: state.validator.phone
  }
};

ValidationResultCard.propTypes = {
  isLoading: PropTypes.bool,
  result: PropTypes.object
};

export default connect(mapStateToProps)(ValidationResultCard)
