import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { validatePhone } from '../store/actions';

export class PhoneValidatorForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleHistoryClick = this.handleHistoryClick.bind(this);
    this.handleHistoryItemClick = this.handleHistoryItemClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.state = {
      input: '',
      showHistory: false
    };
  }

  handleHistoryClick(event) {
    event.stopPropagation();
    event.preventDefault();

    if (this.props.history.length) {
      this.setState({ showHistory: !this.state.showHistory });
    }
  }

  handleHistoryItemClick(input, event) {
    this.setState({ input, showHistory: false })
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.validatePhone(this.state.input);
    this.setState({ input: '' });
  }

  onInputChange(event) {
    const input = event.target.value.trim();
    this.setState({ input });
  }

  buildDropdownMenu(history) {
    return history.map((item) => (
      <div key={item.number}
           className="dropdown-item"
           onClick={this.handleHistoryItemClick.bind(this, item.number)}>
        { item.number }
      </div>
    ));
  }

  render() {
    const { showHistory } = this.state

    return (
      <form className="validator-form card" onSubmit={this.handleSubmit}>
        <div className="card-body">

          <div className="form-group">
            <label>Enter Phone Number</label>
            <div className="input-group">
              <input className="form-control"
                     placeholder="e.g. +852 9123 4567"
                     tabIndex="0" type="text"
                     onChange={this.onInputChange}
                     value={this.state.input} />
              <div className="input-group-append">
                <span className="input-group-text dropdown-select" onClick={this.handleHistoryClick}>
                  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path d="M6.984 9.984h10.03L12 15z" />
                  </svg>
                </span>
              </div>
              <div className={'dropdown-menu ' + (showHistory ? 'show' : '')}>
                { this.buildDropdownMenu(this.props.history) }
              </div>
            </div>
          </div>

          <button className="btn btn-primary"
                  type="submit"
                  disabled={!this.state.input}>
            Validate
          </button>

        </div>
      </form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    validatePhone: input => dispatch(validatePhone(input))
  }
};

const mapStateToProps = (state) => {
  return {
    history: state.validator.history
  }
};

PhoneValidatorForm.propTypes = {
  history: PropTypes.array
};

export default connect(mapStateToProps, mapDispatchToProps)(PhoneValidatorForm)
