import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

const NoHistory = () => {
  return (<span className="muted">No history</span>)
}

const HistoryTable = (history) => {
  if (!history) return null
  const rows = history.map((row) =>
    <tr key={row.number}>
      <td>{row.number}</td>
      <td><code>{row.valid ? 'true' : 'false' }</code></td>
    </tr>
  );

  return (
    <table className="history table">
      <thead>
        <tr>
          <th>Number</th>
          <th>Valid?</th>
        </tr>
      </thead>
      <tbody>
        { rows }
      </tbody>
    </table>
  );
}

export const PhoneHistory = ({ history }) => {
  return (
    <div className="phone-history card">
      <div className="card-body">
        <h2 className="title">Recently validated phone numbers</h2>
        { history.length ? HistoryTable(history) : NoHistory() }
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    history: state.validator.history
  }
};

PhoneHistory.propTypes = {
  isLoading: PropTypes.bool,
  history: PropTypes.array
};

export default connect(mapStateToProps)(PhoneHistory)
