import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import ValidationResultCard from '../ValidationResultCard';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

it('shows message when no data', () => {
  const initialState = { validator: {} };
  const store = mockStore(initialState)
  const wrapper = mount( <Provider store={store}><ValidationResultCard /></Provider> )

  expect(wrapper.find('span.muted').text()).toEqual('No results');
});

it('shows result table when there is data', () => {
  const initialState = {
    validator: {
      phone: {
        "valid":true,
        "number":"85294008901",
        "local_format":"94008901",
        "international_format":"+85294008901",
        "country_prefix":"+852",
        "country_code":"HK",
        "country_name":"Hong Kong, China",
        "location":"",
        "carrier":"SUN Mobile Ltd",
        "line_type":"mobile"
      }
    }
  };
  const store = mockStore(initialState);
  const wrapper = mount(<Provider store={store}><ValidationResultCard /></Provider>);

  expect(wrapper.find('table.result')).toHaveLength(1);
  expect(wrapper.find('tbody').children()).toHaveLength(10);
  expect(wrapper.find('tbody').childAt(0).find('td.key').text()).toEqual('valid');
  expect(wrapper.find('tbody').childAt(1).find('td.key').text()).toEqual('number');
});
