import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import PhoneValidatorForm from '../PhoneValidatorForm';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

it('disables validate button if input is empty', () => {
  const initialState = { validator: { history: [] } };
  const store = mockStore(initialState)
});
