import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import PhoneHistory from '../PhoneHistory';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

it('shows message when no data', () => {
  const initialState = { validator: { history: [] } };
  const store = mockStore(initialState)
  const wrapper = mount( <Provider store={store}><PhoneHistory /></Provider> )

  expect(wrapper.find('span.muted').text()).toEqual('No history');
});

it('shows result table when there is data', () => {
  const initialState = {
    validator: {
      history: [
        {
          "valid":true,
          "number":"85294008901",
        },
        {
          "valid":true,
          "number":"85294008902",
        },
      ]
    }
  };
  const store = mockStore(initialState);
  const wrapper = mount(<Provider store={store}><PhoneHistory /></Provider>);

  expect(wrapper.find('table.history')).toHaveLength(1);
  expect(wrapper.find('tbody').children()).toHaveLength(2);
});
