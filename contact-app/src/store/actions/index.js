import axios from 'axios';
import config from '../../config';

// Action types

export const VALIDATE_PHONE_REQUEST = 'VALIDATE_PHONE_REQUEST';
export const VALIDATE_PHONE_RESPONSE = 'VALIDATE_PHONE_RESPONSE';

/**
 * Validates a single phone number from `contact-service` API.
 * @param {string} number - The phone number to validate
 * @returns {object} action
 */
export function validatePhone(number) {
  const { baseUrl } = config.service;

  return dispatch => {
    dispatch(validatePhoneRequest())
    return axios.get(`${baseUrl}?number=${number}`)
      .then(res => res.data)
      .then(data => dispatch(validatePhoneResponse(data)))
  }
}

export function validatePhoneRequest() {
  return {
    type: VALIDATE_PHONE_REQUEST,
    requestedAt: Date.now()
  }
}

export function validatePhoneResponse(data) {
  return {
    type: VALIDATE_PHONE_RESPONSE,
    data,
    receivedAt: Date.now()
  }
}
