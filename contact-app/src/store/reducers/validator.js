const validator = (state = { history: [] }, action) => {
  switch (action.type) {
    case 'VALIDATE_PHONE_REQUEST':
      return {
        ...state,
        isLoading: true
      };
    case 'VALIDATE_PHONE_RESPONSE':
      return {
        ...state,
        history: state.history.concat([action.data]),
        phone: action.data,
        isLoading: false
      };
    default:
      return state;
  }
};

export default validator;
