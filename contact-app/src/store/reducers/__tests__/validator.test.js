import reducer from '../validator';
import { VALIDATE_PHONE_REQUEST, VALIDATE_PHONE_RESPONSE } from '../../actions';

it('should handle VALIDATE_PHONE_REQUEST', () => {
  expect(reducer({ history: [] }, {
    type: VALIDATE_PHONE_REQUEST
  })).toEqual({
    history: [],
    isLoading: true
  });
});

it('should handle VALIDATE_PHONE_RESPONSE', () => {
  expect(reducer({ history: [] }, {
    type: VALIDATE_PHONE_RESPONSE,
    data: { a: 1, b: 2 }
  })).toEqual({
    phone: { a: 1, b: 2 },
    history: [{ a: 1, b: 2 }],
    isLoading: false
  });
})
