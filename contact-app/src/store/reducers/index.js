import { combineReducers } from 'redux';
import validator from './validator';

export default combineReducers({
  validator
});
