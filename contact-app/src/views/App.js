import React from 'react';
import PhoneHistory from '../components/PhoneHistory';
import PhoneValidatorForm from '../components/PhoneValidatorForm';
import ValidationResultCard from '../components/ValidationResultCard';
import '../styles/App.scss';

function App() {
  return (
    <div className="app">
      <nav className="navbar navbar-expand navbar-dark">
        <div className="brand">Contact App</div>
      </nav>

      <div className="container">
        <main className="main-content">

          <div className="content-title">
            <h1>Phone Number Validator</h1>
            <p className="lead">
              Global phone number validation service, with support for 232 countries.
            </p>
          </div>

          <div className="row">
            <div className="col-md-6">
              <PhoneValidatorForm />
              <ValidationResultCard />
            </div>
            <div className="col-md-6">
              <PhoneHistory />
            </div>
          </div>

        </main>
      </div>
    </div>
  );
}

export default App;
