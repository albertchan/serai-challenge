# Coding Challenge from Serai

## Dependencies

The front end web app requires the following dependencies to build and develop:

- Node.js
- NPM or Yarn

The thin backend service that provides the web app with a validator API is built with Spring Boot. The dependencies are:

- Gradle

## Getting Started

#### Web app

Navigate to `./contact-app` and install the required dependencies:

```sh
cd ./contact-app
yarn install
```

To start the front-end web app in development mode, run:

```sh
yarn run start
```

The app should be opened in a new browser tab and be pointing to [http://localhost:3000/](http://localhost:3000/).

#### Validator service

Navigate to `./contact-service` and run the following command to build and run
the service:

```sh
./gradlew build && java -jar build/libs/contact-service-0.0.1-SNAPSHOT.jar
```

Alternatively, you can build a Docker image of the service:

```sh
docker build -t contact-service .
```

Running the image in interactive mode is done with the following command:

```sh
docker run -it --rm -p 8080:8080 contact-service
```
