package com.seraitrade.contactservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/v1/validate")
public class ValidationController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Environment env;

    @GetMapping(path = "", produces = "application/json")
    public ResponseEntity<Object> validatePhoneNumber(@RequestParam("number") String phoneNumber)
    {
        final String accessKey = env.getProperty("com.numverify.validate.accesskey");
        final String baseUrl = env.getProperty("com.numverify.validate.baseurl");
        final String uri = String.format("%s/validate?access_key=%s&number=%s", baseUrl, accessKey, phoneNumber);

        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(uri, String.class);

        LOGGER.info("get={}", uri);

        return ResponseEntity.ok().body(result);
    }

}
