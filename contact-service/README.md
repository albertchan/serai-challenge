# contact-service

This project provides a thin backend service to provide a phone number
validation API for clients. Validation is provided by a third-party service
called [numverify](https://numverify.com/).

Logging of API calls to all third-party services are saved to a log file.

## Getting Started

To build and the service locally with Gradle, run this command:

```shell script
./gradlew build && java -jar build/libs/contact-service-0.0.1-SNAPSHOT.jar
```

To build a Docker image of the service, run:

```shell script
docker build -t contact-service .
```

Running the image is done with the following command:

```shell script
docker run -it --rm -p 8080:8080 contact-service
```

Note the `--rm` flag removes the stopped container for automatic cleanup.
The `-it` instructs Docker to allocate a pseudo-TTY connected to the
container’s stdin; creating an interactive shell in the container.

Alternatively, you can run the image in detached mode:

```shell script
docker run -d -p 8080:8080 contact-service
```

### Reference Documentation
For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.1.8.RELEASE/gradle-plugin/reference/html/)

### Additional Links
These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)
